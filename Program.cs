﻿using System;
using AdventOfCode2021.Codebase;

namespace AdventOfCode2021
{
	class Program
	{
		static void Main(string[] args)
		{
			MissionBase Today = Engine.GetTodaysMission("Day14");

			var result = Today.Run2();
			Console.WriteLine(result);
		}
	}
}