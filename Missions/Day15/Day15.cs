﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AdventOfCode2021.Codebase;

namespace AdventOfCode2021.Missions.Day15
{
	public class Day15 : MissionBase
	{
		public override string Run()
		{
			int[][] cave = GetInputArraySample().ToArray().Select(x => x.ToArray().Select(y => Convert.ToInt32(y.ToString())).ToArray()).ToArray();
			List<string> pathList = new List<string>() { "0,0" };
			List<string> pathListExpanded = new List<string>() { };
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			bool grown = true;
			int maxPathLenght = cave.Length + cave[0].Length - 1;
			int currentPathDisplay = 0;

			// get all paths, and pray to god there is no step backwards
			while (grown)
			{
				grown = false;

				foreach (var path in pathList)
				{
					var pathSplitted = path.Split("#");
					var currentLength = pathSplitted.Count();

					if (currentLength != currentPathDisplay)
					{
						Console.WriteLine($"Pathlenght : {currentLength} / {maxPathLenght}");
						currentPathDisplay = currentLength;
					}

					var pathsplittedLastNode = pathSplitted.Last().Split(",");

					var lastXCoord = Convert.ToInt32(pathsplittedLastNode[0]);
					var lastYCoord = Convert.ToInt32(pathsplittedLastNode[1]);

					if (lastYCoord != cave.Length - 1)
					{
						pathListExpanded.Add($"{path}#{lastXCoord},{lastYCoord + 1}");
						grown = true;
					}

					if (lastXCoord != cave[0].Length - 1)
					{
						pathListExpanded.Add($"{path}#{lastXCoord + 1},{lastYCoord}");
						grown = true;
					}
				}

				if (pathListExpanded.Count() > 0)
				{
					pathList = pathListExpanded;
					pathListExpanded = new List<string>();
				}
			}

			Console.WriteLine($"All paths found : {pathList.Count()} - in {stopwatch.Elapsed.Seconds} seconds");

			// evaluate paths
			double minPathCost = int.MaxValue;

			foreach (var path in pathList)
			{
				double currentPathCost = 0;

				foreach (var node in path.Split("#"))
				{
					int nodeX = Convert.ToInt32(node.Split(",")[0]);
					int nodeY = Convert.ToInt32(node.Split(",")[1]);
					int currentCost = cave[nodeX][nodeY];
					currentPathCost += currentCost;
				}

				if (currentPathCost < minPathCost)
				{
					minPathCost = currentPathCost;
				}
			}

			Console.WriteLine($"Min path cost found : {minPathCost - cave[0][0]} - in {stopwatch.Elapsed.Seconds} seconds");
			return (minPathCost - cave[0][0]).ToString();
		}
	}
}