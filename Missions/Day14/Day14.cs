﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using AdventOfCode2021.Codebase;
using AdventOfCode2021.Codebase.ExtensionMethods;
using Newtonsoft.Json;

namespace AdventOfCode2021.Missions.Day14
{
	public class Day14 : MissionBase
	{
		public override string Run()
		{
			Stopwatch timer = new Stopwatch();
			timer.Start();
			var polymer = GetInputArray()[0];

			var ruleList = GetInputArray().Skip(2);
			Dictionary<string, string> rules = new Dictionary<string, string>();
			int steps = 10;

			foreach (var rule in ruleList)
			{
				rules.Add(rule.Split(" -> ")[0], rule.Split(" -> ")[1]);
			}

			// flush polymer to File

			string fileName = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "polymer.txt");

			using (var polymerFs = File.Create(fileName))
			using (var polymerSw = new StreamWriter(polymerFs)) { polymerSw.Write(polymer); }

			string fileNameNext = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "polymerNext.txt");

			// steps
			for (int stepCounter = 0; stepCounter < steps; stepCounter++)
			{
				using (var polymerFsNext = File.Create(fileNameNext)) { }

				using (var polymerFs = File.OpenRead(fileName))
				using (var polymerSr = new StreamReader(polymerFs))
				{
					char[] polymerBuffer = null;

					string carryOverChar = string.Empty;

					while (polymerSr.Peek() >= 0)
					{
						polymerBuffer = new char[100000];
						polymerSr.Read(polymerBuffer, 0, polymerBuffer.Length);
						polymer = carryOverChar + String.Join("", polymerBuffer.Where(x => x != 0));

						// start working on polymer

						//using (var polymerFsNext = File.OpenWrite(fileNameNext))
						//using (var polymerSwNext = new StreamWriter(polymerFsNext))
						using (var polymerSwNext = File.AppendText(fileNameNext))
						{
							for (int i = 0; i < polymer.Length - 1; i++)
							{
								var compound = $"{polymer[i]}{polymer[i + 1]}";

								if (rules.Keys.Contains(compound))
								{
									if (i == 0 && carryOverChar != "" && polymer[i] == carryOverChar?[0])
									{
										polymerSwNext.Write($"{rules[compound]}");
									}
									else
									{
										polymerSwNext.Write($"{polymer[i]}{rules[compound]}");
									}
								}
								else
								{
									polymerSwNext.Write($"{polymer[i]}");
								}
							}

							polymerSwNext.Write(polymer.Last());
							//File.WriteAllText(fileName, "");
						}

						carryOverChar = polymer.Last().ToString();
						// step back 1 here if you can
					}
				}
				// stop working on polymer

				File.Delete(fileName);
				File.Move(fileNameNext, fileName);

				Console.WriteLine($"step : {(stepCounter + 1).ToString("D2")}");
			}

			Console.WriteLine($"Polymer built in {timer.ElapsedMilliseconds / 1000}sec ");
			File.Delete(fileNameNext);

			Dictionary<char, double> occurences = new Dictionary<char, double>();

			using (var polymerFs = File.OpenRead(fileName))
			using (var polymerSr = new StreamReader(polymerFs))
			{
				while (polymerSr.Peek() >= 0)
				{
					char[] polymerBuffer = new char[10];
					polymerSr.Read(polymerBuffer, 0, polymerBuffer.Length);
					//polymer = polymerSr.ReadToEnd();
					polymer = String.Join("", polymerBuffer.Where(x => x != 0));
					List<char> uniques = polymer.Distinct().ToList();

					foreach (var element in uniques)
					{
						if (occurences.ContainsKey(element))
						{
							occurences[element] += polymer.Count(x => x == element);
						}
						else
						{
							occurences.Add(element, polymer.Count(x => x == element));
						}
					}
				}
			}

			timer.Stop();
			//File.Delete(fileName);
			return $"Result found in {timer.ElapsedMilliseconds / 1000}sec : {(occurences.Values.Max() - occurences.Values.Min()).ToString()}";
		}

		public override string Run2()
		{
			Stopwatch timer = new Stopwatch();
			timer.Start();
			string polymer = GetInputArray()[0];
			int steps = 40;

			List<string> ruleList = GetInputArray().Skip(2).ToList();
			Dictionary<string, string> rules = new Dictionary<string, string>();
			Dictionary<string, Tuple<string, string>> newCompounds = new Dictionary<string, Tuple<string, string>>();

			Dictionary<char, double> letterCount = new Dictionary<char, double>();
			Dictionary<string, double> compoundCount = new Dictionary<string, double>();
			Dictionary<string, double> compoundCountNext = new Dictionary<string, double>();

			foreach (var rule in ruleList)
			{
				var compound = rule.Split(" -> ")[0];
				var newLetter = rule.Split(" -> ")[1];
				rules.Add(compound, newLetter);
				newCompounds.Add(compound, new Tuple<string, string>($"{compound[0]}{newLetter}", $"{newLetter}{compound[1]}"));
			}

			foreach (var letter in polymer)
			{
				letterCount.AddOrIncrement(letter);
			}

			for (int i = 0; i < polymer.Count() - 1; i++)
			{
				compoundCount.AddOrIncrement($"{polymer[i]}{polymer[i + 1]}");
			}

			for (int stepCount = 0; stepCount < steps; stepCount++)
			{
				Console.WriteLine($"Step : {stepCount+1}");
				
				foreach (var compountEntry in compoundCount)
				{
					string compound = compountEntry.Key;
					double count = compountEntry.Value;

					letterCount.AddOrIncrementBy(rules[compound][0], count);
					compoundCountNext.AddOrIncrementBy(newCompounds[compound].Item1, count);
					compoundCountNext.AddOrIncrementBy(newCompounds[compound].Item2, count);
				}
				
				compoundCount = JsonConvert.DeserializeObject<Dictionary<string, double>>(JsonConvert.SerializeObject(compoundCountNext));
				compoundCountNext = new Dictionary<string, double>();
			}

			var maxLetterCount = letterCount.OrderByDescending(x => x.Value).First();
			var minLetterCount = letterCount.OrderBy(x => x.Value).First();

			return
				$"Letters : Max:{maxLetterCount.Key} - {maxLetterCount.Value}  Min: {minLetterCount.Key} - {minLetterCount.Value}    - Result : {maxLetterCount.Value - minLetterCount.Value}";
		}
	}
}