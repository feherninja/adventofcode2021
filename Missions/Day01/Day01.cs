﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdventOfCode2021.Codebase;

namespace AdventOfCode2021.Missions.Day01
{
	public class Day01 : MissionBase
	{
		public override string Run()
		{
			List<int> Inputs = GetInputArray().Select(x => Convert.ToInt32(x)).ToList();
			int increase = 0;

			for (int i = 1; i < Inputs.Count; i++)
			{
				if (Inputs[i] > Inputs[i - 1])
				{
					increase++;
				}
			}

			return increase.ToString();
		}

		public override string Run2()
		{
			List<int> Inputs = GetInputArray().Select(x => Convert.ToInt32(x)).ToList();
			Dictionary<int, int> sums3 = new Dictionary<int, int>();
			int increase3 = 0;

			for (int i = 0; i < Inputs.Count - 2; i++)
			{
				sums3[i] = Inputs[i] + Inputs[i + 1] + Inputs[i + 2];
			}

			List<int> measurements3 = sums3.Values.ToList();

			for (int i = 1; i < measurements3.Count; i++)
			{
				if (measurements3[i] > measurements3[i - 1])
				{
					increase3++;
				}
			}

			return increase3.ToString();
		}
	}
}