﻿namespace AdventOfCode2021.Missions.Day11.Models
{
	public class Octopus
	{
		public int Value { get; set; }
		public bool Flashed { get; set; }

		public Octopus(int value, bool flashed)
		{
			Value = value;
			Flashed = flashed;
		}

		public static Octopus operator ++(Octopus a)
		{
			return new Octopus(!a.Flashed?a.Value+1:a.Value, a.Flashed);
		}

		public static bool operator <(Octopus a, int b)
		{
			return a.Value < b;
		}

		public static bool operator >(Octopus a, int b)
		{
			return a.Value > b;
		}

		public static int operator -(Octopus a, int b)
		{
			return a.Value - b;
		}

		public override string ToString()
		{
			return Value.ToString();
		}
	}
}