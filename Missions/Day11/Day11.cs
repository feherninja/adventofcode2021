﻿using System;
using System.Linq;
using AdventOfCode2021.Codebase;
using AdventOfCode2021.Missions.Day11.Models;

namespace AdventOfCode2021.Missions.Day11
{
	public class Day11 : MissionBase
	{
		public override string Run()
		{
			var swarm = GetInputArray().ToArray().Select(x => x.ToArray().Select(y => new Octopus(Convert.ToInt32(y.ToString()), false)).ToArray())
			                           .ToArray();

			int flashCount = 0;
			int steps = 100;

			for (int stepIterator = 0; stepIterator < steps; stepIterator++)
			{
				// increase values
				for (int i = 0; i < swarm.Length; i++)
				{
					for (int j = 0; j < swarm[0].Length; j++)
					{
						swarm[i][j]++;
					}
				}

				// flash the shit out of the cave (HM05)
				bool needMoreFlash = swarm.SelectMany(x => x).Where(x => !x.Flashed)?.Select(x => x.Value).Count(x => x > 9) > 0;

				while (needMoreFlash)
				{
					needMoreFlash = false;

					for (int i = 0; i < swarm.Length; i++)
					{
						for (int j = 0; j < swarm[0].Length; j++)
						{
							if (swarm[i][j] > 9 && !swarm[i][j].Flashed)
							{
								if (i > 0) swarm[i - 1][j]++; //up
								if (i < swarm.Length - 1) swarm[i + 1][j]++; //down
								if (j > 0) swarm[i][j - 1]++; //left
								if (j < swarm[0].Length - 1) swarm[i][j + 1]++; //right
								if (i > 0 && j > 0) swarm[i - 1][j - 1]++; //upperleft
								if (i > 0 && j < swarm[0].Length - 1) swarm[i - 1][j + 1]++; //upperright
								if (i < swarm.Length - 1 && j > 0) swarm[i + 1][j - 1]++; //lowerleft
								if (i < swarm.Length - 1 && j < swarm[0].Length - 1) swarm[i + 1][j + 1]++; //lowerright

								swarm[i][j].Flashed = true;
								swarm[i][j].Value = 0;
								flashCount++;
								needMoreFlash = true;
							}
						}
					}
				}

				// reset swarm flash
				for (int i = 0; i < swarm.Length; i++)
				{
					for (int j = 0; j < swarm[0].Length; j++)
					{
						swarm[i][j].Flashed = false;
					}
				}
			}

			return flashCount.ToString();
		}

		public override string Run2()
		{
			var swarm = GetInputArray().ToArray().Select(x => x.ToArray().Select(y => new Octopus(Convert.ToInt32(y.ToString()), false)).ToArray())
			                           .ToArray();

			int flashCount = 0;
			int steps = 100;
			bool synchronizedFound = false;
			int foundstep = 0;

			for (int stepIterator = 0; !synchronizedFound; stepIterator++)
			{
				// increase values
				for (int i = 0; i < swarm.Length; i++)
				{
					for (int j = 0; j < swarm[0].Length; j++)
					{
						swarm[i][j]++;
					}
				}

				// flash the shit out of the cave (HM05)
				bool needMoreFlash = swarm.SelectMany(x => x).Where(x => !x.Flashed)?.Select(x => x.Value).Count(x => x > 9) > 0;

				while (needMoreFlash)
				{
					needMoreFlash = false;

					for (int i = 0; i < swarm.Length; i++)
					{
						for (int j = 0; j < swarm[0].Length; j++)
						{
							if (swarm[i][j] > 9 && !swarm[i][j].Flashed)
							{
								if (i > 0) swarm[i - 1][j]++; //up
								if (i < swarm.Length - 1) swarm[i + 1][j]++; //down
								if (j > 0) swarm[i][j - 1]++; //left
								if (j < swarm[0].Length - 1) swarm[i][j + 1]++; //right
								if (i > 0 && j > 0) swarm[i - 1][j - 1]++; //upperleft
								if (i > 0 && j < swarm[0].Length - 1) swarm[i - 1][j + 1]++; //upperright
								if (i < swarm.Length - 1 && j > 0) swarm[i + 1][j - 1]++; //lowerleft
								if (i < swarm.Length - 1 && j < swarm[0].Length - 1) swarm[i + 1][j + 1]++; //lowerright

								swarm[i][j].Flashed = true;
								swarm[i][j].Value = 0;
								flashCount++;
								needMoreFlash = true;
							}
						}
					}
				}

				// reset swarm flash and check for full flash
				if (swarm.SelectMany(x => x).Count(x => x.Flashed) == swarm.SelectMany(x => x).Count())
				{
					synchronizedFound = true;
					foundstep = stepIterator;
				}

				for (int i = 0; i < swarm.Length; i++)
				{
					for (int j = 0; j < swarm[0].Length; j++)
					{
						swarm[i][j].Flashed = false;
					}
				}
			}

			return (foundstep + 1).ToString();
		}

		private void displaySwarm(Octopus[][] swarm, bool highlight = false, int x = 0, int y = 0)
		{
			for (int i = 0; i < swarm.Length; i++)
			{
				for (int j = 0; j < swarm[0].Length; j++)
				{
					if (swarm[i][j] > 9)
					{
						Console.ForegroundColor = (highlight && i == x && j == y) ? ConsoleColor.DarkRed : ConsoleColor.DarkGreen;
						Console.Write(swarm[i][j].Value - 10);
					}
					else
					{
						Console.ForegroundColor = (highlight && i == x && j == y) ? ConsoleColor.DarkRed : ConsoleColor.White;
						Console.Write(swarm[i][j]);
					}
				}

				Console.WriteLine();
			}
		}
	}
}