﻿using System.Collections.Generic;

namespace AdventOfCode2021.Missions.Day12.Models
{
	public class Node
	{
		public string Name { get; set; }
		bool Tapped { get; set; }
		public List<Node> ConnectedNodes { get; set; }

		public Node(string label)
		{
			Name = label;
			ConnectedNodes = new List<Node>();
			Tapped = false;
		}
	}
}