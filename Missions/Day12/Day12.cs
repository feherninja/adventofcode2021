﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdventOfCode2021.Codebase;
using AdventOfCode2021.Missions.Day12.Models;

namespace AdventOfCode2021.Missions.Day12
{
	public class Day12 : MissionBase
	{
		public override string Run()
		{
			var graphInput = GetInputArray();
			var graph = new List<Node>();
			var pathList = new List<string>();
			var nextPathList = new List<string>();

			// build the graph
			foreach (var graphConnection in graphInput)
			{
				var nodeNames = graphConnection.Split("-").ToList();

				bool new1 = !graph.Select(x => x.Name).Contains(nodeNames[0]);
				bool new2 = !graph.Select(x => x.Name).Contains(nodeNames[1]);

				Node node1 = new1 ? new Node(nodeNames[0]) : graph.First(x => x.Name == nodeNames[0]);
				Node node2 = new2 ? new Node(nodeNames[1]) : graph.First(x => x.Name == nodeNames[1]);

				node1.ConnectedNodes.Add(node2);
				node2.ConnectedNodes.Add(node1);

				if (new1) graph.Add(node1);
				if (new2) graph.Add(node2);
			}

			// shred my last piece of sanity with this shit
			Node head = graph.First(x => x.Name == "start");
			pathList.Add(head.Name);
			bool added = true;

			while (added)
			{
				added = false;

				foreach (var path in pathList)
				{
					Console.WriteLine(path);
					string lastNodeName = path.Split(",").ToList().Last();
					string prevNodeName = path.Split(",").Length > 1 ? path.Split(",").ToList()[^2] : "start";
					Node lastNode = graph.FirstOrDefault(x => x.Name == lastNodeName);

					if (lastNodeName == "end")
					{
						nextPathList.Add(path);
					}
					else if (lastNode.ConnectedNodes.Where(x => x.Name != "start").Count() > 0)
					{
						foreach (var connectedToLastNode in lastNode.ConnectedNodes.Where(x => x.Name != prevNodeName.ToLower()))
						{
							if (connectedToLastNode.Name != "start" && !path.Contains($",{connectedToLastNode.Name.ToLower()},"))
							{
								nextPathList.Add($"{path},{connectedToLastNode.Name}");
								added = true;
							}
						}
					}
				}

				pathList = nextPathList.Distinct().ToList();
				nextPathList = new List<string>();
			}

			return pathList.Count.ToString();
		}

		public override string Run2()
		{
			var graphInput = GetInputArray();
			var graph = new List<Node>();
			var pathList = new List<string>();
			var nextPathList = new List<string>();

			// build the graph
			foreach (var graphConnection in graphInput)
			{
				var nodeNames = graphConnection.Split("-").ToList();

				bool new1 = !graph.Select(x => x.Name).Contains(nodeNames[0]);
				bool new2 = !graph.Select(x => x.Name).Contains(nodeNames[1]);

				Node node1 = new1 ? new Node(nodeNames[0]) : graph.First(x => x.Name == nodeNames[0]);
				Node node2 = new2 ? new Node(nodeNames[1]) : graph.First(x => x.Name == nodeNames[1]);

				node1.ConnectedNodes.Add(node2);
				node2.ConnectedNodes.Add(node1);

				if (new1) graph.Add(node1);
				if (new2) graph.Add(node2);
			}

			// shred my last piece of sanity with this shit
			Node head = graph.First(x => x.Name == "start");
			pathList.Add(head.Name);
			bool added = true;

			while (added)
			{
				added = false;

				foreach (var path in pathList)
				{
					string lastNodeName = path.Split(",").ToList().Last();
					string prevNodeName = path.Split(",").Length > 1 ? path.Split(",").ToList()[^2] : "start";
					Node lastNode = graph.FirstOrDefault(x => x.Name == lastNodeName);

					if (lastNodeName == "end")
					{
						nextPathList.Add(path);
					}
					else if (lastNode.ConnectedNodes.Where(x => x.Name != "start").Count() > 0)
					{
						foreach (var connectedToLastNode in lastNode.ConnectedNodes)
						{
							if (path == "start,A,b,A" && connectedToLastNode.Name == "b")
							{
								Console.WriteLine();
							}

							if (connectedToLastNode.Name != "start" &&
							    (
								    path.Split(",").Where(x => x.All(y => char.IsLower(y))).Distinct().Count() ==
								    path.Split(",").Where(x => x.All(y => char.IsLower(y))).Count() ||
								    !path.Contains($",{connectedToLastNode.Name.ToLower()},")
							    )
							   )
							{
								nextPathList.Add($"{path},{connectedToLastNode.Name}");
								added = true;
							}
						}
					}
				}

				pathList = nextPathList.Distinct().ToList();
				nextPathList = new List<string>();
			}

			return pathList.Count.ToString();
		}
	}
}