﻿using System.Collections.Generic;
using System.Linq;
using AdventOfCode2021.Codebase;

namespace AdventOfCode2021.Missions.Day10
{
	public class Day10 : MissionBase
	{
		private char getCloser(char opener)
		{
			char closer = ' ';

			switch (opener)
			{
				case '(':
					closer = ')';
					break;
				case '[':
					closer = ']';
					break;
				case '{':
					closer = '}';
					break;
				case '<':
					closer = '>';
					break;
			}

			return closer;
		}

		private bool isCloser(char susChar)
		{
			return susChar == ']' || susChar == ')' || susChar == '}' || susChar == '>';
		}

		private int susValue(char illegal)
		{
			switch (illegal)
			{
				case ')':
					return 3;
				case ']':
					return 57;
				case '}':
					return 1197;
				case '>':
					return 25137;
				default:
					return 0;
			}
		}

		private int closerValue(char opener)
		{
			switch (opener)
			{
				case ')':
					return 1;
				case ']':
					return 2;
				case '}':
					return 3;
				case '>':
					return 4;
				default:
					return 0;
			}
		}

		public override string Run()
		{
			var input = GetInputArray();
			var susSum = 0;
			Dictionary<string, char> guiltyLines = new Dictionary<string, char>();
			Dictionary<string, int> guiltyValues = new Dictionary<string, int>();

			foreach (var line in input)
			{
				bool found = true;
				string source = new string(line);

				while (found && source.Length > 1)
				{
					found = false;

					for (int i = 0; i < source.Length; i++)
					{
						if (i < source.Length - 1 && source[i + 1] == getCloser(source[i]))
						{
							found = true;
							source = source.Remove(i, 2);

							break;
						}
					}
				}

				var firstIllegalCloser = source.FirstOrDefault(x => isCloser(x));

				if (firstIllegalCloser != '\0')
				{
					var susLevel = susValue(firstIllegalCloser);
					susSum += susLevel;
					guiltyLines.Add(line, firstIllegalCloser);
					guiltyValues.Add(line, susLevel);
				}
			}

			return susSum.ToString();
		}

		public override string Run2()
		{
			var input = GetInputArray();
			List<string> incompleteLines = new List<string>();
			List<double> completionValues = new List<double>();

			#region getIncompleteLines

			foreach (var line in input)
			{
				bool found = true;
				string source = new string(line);

				while (found && source.Length > 1)
				{
					found = false;

					for (int i = 0; i < source.Length; i++)
					{
						if (i < source.Length - 1 && source[i + 1] == getCloser(source[i]))
						{
							found = true;
							source = source.Remove(i, 2);

							break;
						}
					}
				}

				var firstIllegalCloser = source.FirstOrDefault(x => isCloser(x));

				if (firstIllegalCloser == '\0')
				{
					incompleteLines.Add(source);
				}
			}

			#endregion getIncompleteLines

			foreach (var line in incompleteLines)
			{
				double lineCompletionCost = 0;

				foreach (var opener in line.Reverse())
				{
					var closerNumber = closerValue(getCloser(opener));
					lineCompletionCost = lineCompletionCost * 5 + closerNumber;
				}

				completionValues.Add(lineCompletionCost);
			}

			var median = completionValues.OrderBy(x => x).ToList()[completionValues.Count / 2];
			return median.ToString();
		}
	}
}