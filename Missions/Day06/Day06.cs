﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AdventOfCode2021.Codebase;
using AdventOfCode2021.Missions.Day06.Models;

namespace AdventOfCode2021.Missions.Day06
{
	public class Day06 : MissionBase
	{
		public override string Run()
		{
			List<int> fishies = GetInput().Split(",").Select(x => Convert.ToInt32(x)).ToList();

			for (int day = 0; day < 80; day++)
			{
				Console.WriteLine(day);
				int i = 0;

				while (i < fishies.Count())
				{
					if (fishies[i] == 0)
					{
						fishies[i] = 6;
						fishies.Add(9);
					}
					else
					{
						fishies[i]--;
					}

					i++;
				}
			}

			return $"{fishies.Count()}";
		}

		public override string Run2()
		{
			var input = GetInput();
			//string input = "3,4,3,1,2";
			Dictionary<int, double> fishTimerCollection = new Dictionary<int, double>()
			{
				{ 0, 0 },
				{ 1, 0 },
				{ 2, 0 },
				{ 3, 0 },
				{ 4, 0 },
				{ 5, 0 },
				{ 6, 0 },
				{ 7, 0 },
				{ 8, 0 }
			};

			foreach (var fishy in input.Split(",").Select(x => Convert.ToInt32(x)))
			{
				fishTimerCollection[fishy]++;
			}

			for (int day = 0; day < 256; day++)
			{
				Console.WriteLine(day);
				Dictionary<int, double> fishTimerCollectionTomorrow = new Dictionary<int, double>()
				{
					{ 0, 0 },
					{ 1, 0 },
					{ 2, 0 },
					{ 3, 0 },
					{ 4, 0 },
					{ 5, 0 },
					{ 6, 0 },
					{ 7, 0 },
					{ 8, 0 }
				};

				double newFishy = fishTimerCollection[0];
				fishTimerCollectionTomorrow[0] = fishTimerCollection[1];
				fishTimerCollectionTomorrow[1] = fishTimerCollection[2];
				fishTimerCollectionTomorrow[2] = fishTimerCollection[3];
				fishTimerCollectionTomorrow[3] = fishTimerCollection[4];
				fishTimerCollectionTomorrow[4] = fishTimerCollection[5];
				fishTimerCollectionTomorrow[5] = fishTimerCollection[6];
				fishTimerCollectionTomorrow[6] = fishTimerCollection[7];
				fishTimerCollectionTomorrow[7] = fishTimerCollection[8];
				fishTimerCollectionTomorrow[8] = newFishy;
				fishTimerCollectionTomorrow[6] += newFishy;

				fishTimerCollection = fishTimerCollectionTomorrow;
			}

			return $"{fishTimerCollection.Values.Sum()}";
		}

		// why linked list wont work (hint: out-of-memory on day ~156)
		public string Run3()
		{
			List<byte> input = GetInput().Split(",").Select(x => Convert.ToByte(x)).ToList();
			//List<int> input = new List<int>() { 3, 4, 3, 1, 2 };
			Fishy tail = new Fishy();
			Fishy head = new Fishy();
			Fishy prev = new Fishy();
			Fishy next = new Fishy();

			// create linked list
			for (int i = 0; i < input.Count; i++)
			{
				var current = new Fishy(input[i]);

				if (i == 0)
				{
					head = current;
					//head.Head = true;
				}
				else
				{
					//current.Prev = prev;
					prev.Next = current;
				}

				if (i == input.Count() - 1)
				{
					tail = current;
					//tail.Tail = true;
				}

				prev = current;
			}

			// play god
			for (int day = 0; day < 256; day++)
			{
				Console.WriteLine(day);
				Fishy current = head;

				while (current != null)
				{
					if (current.Timer == 0)
					{
						current.Timer = 6;
						Fishy baby = new Fishy(9);
						tail.Next = baby;
						//baby.Prev = tail;
						//tail.Tail = false;
						//baby.Tail = true;
						tail = baby;
					}
					else
					{
						current.Timer--;
					}

					current = current.Next;
				}
			}

			int count = 0;
			Fishy counter = head;

			while (counter != null)
			{
				count++;
				counter = counter.Next;
			}

			return $"{count}";
		}

		// why stringBuilder wont work (hint: out-of-memory on day ~148)
		public string Run4()
		{
			string current = GetInput();
			//string current = "3,4,3,1,2";
			string next = "";

			for (int day = 0; day < 256; day++)
			{
				Console.WriteLine(day);
				StringBuilder sb = new StringBuilder();

				int newFishCounter = 0;

				foreach (char currentChar in current)
				{
					if (currentChar == ',')
					{
						sb.Append(",");
					}
					else if (currentChar == '0')
					{
						sb.Append("6");
						newFishCounter++;
					}
					else
					{
						sb.Append($"{Convert.ToInt32(currentChar.ToString()) - 1}");
					}
				}

				for (int i = 0; i < newFishCounter; i++)
				{
					sb.Append(",8");
				}

				next = sb.ToString();
				current = next;
			}

			return $"{current.Split(",").Count()}";
		}
	}
}