﻿namespace AdventOfCode2021.Missions.Day06.Models
{
	public class Fishy
	{
		public byte Timer { get; set; }
		//public bool Head { get; set; }
		//public bool Tail { get; set; }
		//public Fishy Prev { get; set; }
		public Fishy Next { get; set; }

		public Fishy(byte timer = 0)
		{
			Timer = timer;
			//Head = false;
			//Tail = false;
		}
	}
}