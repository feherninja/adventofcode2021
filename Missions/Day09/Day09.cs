﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using AdventOfCode2021.Codebase;

namespace AdventOfCode2021.Missions.Day09
{
	[SuppressMessage("ReSharper", "ReplaceWithSingleAssignment.True")]
	public class Day09 : MissionBase
	{
		public override string Run()
		{
			int[][] heightmap = GetInputArray().ToArray().Select(x => x.ToArray().Select(y => Convert.ToInt32(y.ToString())).ToArray()).ToArray();

			int riskLevelSum = 0;

			for (int i = 0; i < heightmap.Length; i++)
			{
				for (int j = 0; j < heightmap[0].Length; j++)
				{
					bool lowPoint = true;

					if (i > 0 && heightmap[i][j] >= heightmap[i - 1][j])
					{
						lowPoint = false;
					}

					if (j > 0 && heightmap[i][j] >= heightmap[i][j - 1])
					{
						lowPoint = false;
					}

					if (i < heightmap.Length - 1 && heightmap[i][j] >= heightmap[i + 1][j])
					{
						lowPoint = false;
					}

					if (j < heightmap[0].Length - 1 && heightmap[i][j] >= heightmap[i][j + 1])
					{
						lowPoint = false;
					}

					if (lowPoint)
					{
						riskLevelSum += heightmap[i][j] + 1;
					}
				}
			}

			return riskLevelSum.ToString();
		}

		public override string Run2()
		{
			int[][] heightmap = GetInputArray().ToArray().Select(x => x.ToArray().Select(y => Convert.ToInt32(y.ToString())).ToArray()).ToArray();

			List<Tuple<int, int>> lowPoints = new List<Tuple<int, int>>();
			List<Tuple<int, int>> cluster;
			List<Tuple<int, int>> susPoints;
			List<Tuple<int, int>> nextSusPoints;
			List<int> clusterSizes = new List<int>();

			for (int i = 0; i < heightmap.Length; i++)
			{
				for (int j = 0; j < heightmap[0].Length; j++)
				{
					bool lowPoint = true;

					if (i > 0 && heightmap[i][j] >= heightmap[i - 1][j])
					{
						lowPoint = false;
					}

					if (j > 0 && heightmap[i][j] >= heightmap[i][j - 1])
					{
						lowPoint = false;
					}

					if (i < heightmap.Length - 1 && heightmap[i][j] >= heightmap[i + 1][j])
					{
						lowPoint = false;
					}

					if (j < heightmap[0].Length - 1 && heightmap[i][j] >= heightmap[i][j + 1])
					{
						lowPoint = false;
					}

					if (lowPoint)
					{
						lowPoints.Add(new Tuple<int, int>(i, j));
					}
				}
			}

			foreach (var lowPoint in lowPoints)
			{
				cluster = new List<Tuple<int, int>>() { };
				susPoints = new List<Tuple<int, int>>() { lowPoint };
				nextSusPoints = new List<Tuple<int, int>>();
				bool newPointsAdded = true;

				while (newPointsAdded)
				{
					newPointsAdded = false;

					foreach (var susPoint in susPoints)
					{
						if (susPoint.Item1 > 0 &&
						    heightmap[susPoint.Item1 - 1][susPoint.Item2] > heightmap[susPoint.Item1][susPoint.Item2] &&
						    heightmap[susPoint.Item1 - 1][susPoint.Item2] != 9 &&
						    !cluster.Contains(new Tuple<int, int>(susPoint.Item1 - 1, susPoint.Item2)) &&
						    !susPoints.Contains(new Tuple<int, int>(susPoint.Item1 - 1, susPoint.Item2)) &&
						    !nextSusPoints.Contains(new Tuple<int, int>(susPoint.Item1 - 1, susPoint.Item2)))
						{
							nextSusPoints.Add(new Tuple<int, int>(susPoint.Item1 - 1, susPoint.Item2));
							newPointsAdded = true;
						}

						if (susPoint.Item1 < heightmap.Length - 1 &&
						    heightmap[susPoint.Item1 + 1][susPoint.Item2] > heightmap[susPoint.Item1][susPoint.Item2] &&
						    heightmap[susPoint.Item1 + 1][susPoint.Item2] != 9 &&
						    !cluster.Contains(new Tuple<int, int>(susPoint.Item1 + 1, susPoint.Item2)) &&
						    !susPoints.Contains(new Tuple<int, int>(susPoint.Item1 + 1, susPoint.Item2)) &&
						    !nextSusPoints.Contains(new Tuple<int, int>(susPoint.Item1 + 1, susPoint.Item2)))
						{
							nextSusPoints.Add(new Tuple<int, int>(susPoint.Item1 + 1, susPoint.Item2));
							newPointsAdded = true;
						}

						if (susPoint.Item2 > 0 &&
						    heightmap[susPoint.Item1][susPoint.Item2 - 1] > heightmap[susPoint.Item1][susPoint.Item2] &&
						    heightmap[susPoint.Item1][susPoint.Item2 - 1] != 9 &&
						    !cluster.Contains(new Tuple<int, int>(susPoint.Item1, susPoint.Item2 - 1)) &&
						    !susPoints.Contains(new Tuple<int, int>(susPoint.Item1, susPoint.Item2 - 1)) &&
						    !nextSusPoints.Contains(new Tuple<int, int>(susPoint.Item1 - 1, susPoint.Item2 - 1)))
						{
							nextSusPoints.Add(new Tuple<int, int>(susPoint.Item1, susPoint.Item2 - 1));
							newPointsAdded = true;
						}

						if (susPoint.Item2 < heightmap[0].Length - 1 &&
						    heightmap[susPoint.Item1][susPoint.Item2 + 1] > heightmap[susPoint.Item1][susPoint.Item2] &&
						    heightmap[susPoint.Item1][susPoint.Item2 + 1] != 9 &&
						    !cluster.Contains(new Tuple<int, int>(susPoint.Item1, susPoint.Item2 + 1)) &&
						    !susPoints.Contains(new Tuple<int, int>(susPoint.Item1, susPoint.Item2 + 1)) &&
						    !nextSusPoints.Contains(new Tuple<int, int>(susPoint.Item1, susPoint.Item2 + 1)))
						{
							nextSusPoints.Add(new Tuple<int, int>(susPoint.Item1, susPoint.Item2 + 1));
							newPointsAdded = true;
						}
					}

					cluster.AddRange(susPoints);
					susPoints = new List<Tuple<int, int>>(nextSusPoints);
					nextSusPoints = new List<Tuple<int, int>>();
				}

				clusterSizes.Add(cluster.Count);
			}

			var top3 = clusterSizes.OrderByDescending(x => x).Take(3).ToList();
			return $"{string.Join(" ", top3)} agrr = {top3.Aggregate((total, next) => total * next)}";
		}
	}
}