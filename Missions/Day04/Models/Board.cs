﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Missions.Day04.Models
{
	public class Board
	{
		private int[][] numbers;
		public bool Bingo => CheckBingo();

		public Board(IEnumerable<string> input)
		{
			numbers = new int[5][];

			for (int i = 0; i < input.Count(); i++)
			{
				numbers[i] = input.ToList()[i].Split(" ").Where(x => x != "").Select(x => Convert.ToInt32(x)).ToArray();
			}
		}

		public bool StrikeOut(int number)
		{
			for (int i = 0; i < numbers.Length; i++)
			{
				for (int j = 0; j < numbers[0].Length; j++)
				{
					if (numbers[i][j] == number)
					{
						numbers[i][j] = 0;
					}
				}
			}

			return CheckBingo();
		}

		private bool CheckBingo()
		{
			for (int i = 0; i < numbers.Length; i++)
			{
				if (numbers[i][0] == numbers[i][1] &&
				    numbers[i][0] == numbers[i][2] &&
				    numbers[i][0] == numbers[i][3] &&
				    numbers[i][0] == numbers[i][4])
				{
					return true;
				}

				if (numbers[0][i] == numbers[1][i] &&
				    numbers[0][i] == numbers[2][i] &&
				    numbers[0][i] == numbers[3][i] &&
				    numbers[0][i] == numbers[4][i])
				{
					return true;
				}
			}

			return false;
		}

		public int GetSum()
		{
			return numbers.SelectMany(x => x).Sum();
		}
	}
}