﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdventOfCode2021.Codebase;
using AdventOfCode2021.Missions.Day04.Models;
using MoreLinq;

namespace AdventOfCode2021.Missions.Day04
{
	public class Day04 : MissionBase
	{
		public override string Run()
		{
			List<int> draws = GetInputArray()[0].Split(",").Select(x => Convert.ToInt32(x)).ToList();

			var boardsRaw = GetInputArray().Skip(2).ToList();
			boardsRaw.RemoveAll(x => x == string.Empty);

			List<Board> boards = boardsRaw.Batch(5).Select(x => new Board(x)).ToList();
			bool found = false;
			int winnerNo = 0;
			Board winnerBoard = null;

			for (int i = 0; i < draws.Count; i++)
			{
				if (found)
				{
					break;
				}

				for (int j = 0; j < boards.Count; j++)
				{
					boards[j].StrikeOut(draws[i]);

					if (boards[j].Bingo)
					{
						found = true;
						winnerNo = draws[i];
						winnerBoard = boards[j];
						break;
					}
				}
			}

			return (winnerBoard.GetSum() * winnerNo).ToString();
		}

		public override string Run2()
		{
			List<int> draws = GetInputArray()[0].Split(",").Select(x => Convert.ToInt32(x)).ToList();

			var boardsRaw = GetInputArray().Skip(2).ToList();
			boardsRaw.RemoveAll(x => x == string.Empty);

			List<Board> boards = boardsRaw.Batch(5).Select(x => new Board(x)).ToList();
			bool found = false;
			int winnerNo = 0;
			Board loserBoard = null;

			for (int i = 0; i < draws.Count; i++)
			{
				if (found)
				{
					break;
				}

				for (int j = 0; j < boards.Count; j++)
				{
					boards[j].StrikeOut(draws[i]);

					if (boards.Where(x => !x.Bingo).Count() == 1)
					{
						loserBoard = boards.First(x => !x.Bingo);
					}

					if (loserBoard?.Bingo == true)
					{
						found = true;
						winnerNo = draws[i];
						break;
					}
				}
			}

			return (loserBoard.GetSum() * winnerNo).ToString();
		}
	}
}