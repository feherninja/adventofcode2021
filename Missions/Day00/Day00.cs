﻿using AdventOfCode2021.Codebase;

namespace AdventOfCode2021.Missions.Day00
{
	public class Day00 : MissionBase
	{
		public override string Run()
		{
			return GetInputOnline().Result;
		}
		
		
	}
}