﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdventOfCode2021.Codebase;

namespace AdventOfCode2021.Missions.Day08
{
	public class Day08 : MissionBase
	{
		public override string Run()
		{
			var input = GetInputArray();
			var displays = input.Select(x => x.Split(" | ")[1]);
			var sum = 0;

			foreach (var display in displays)
			{
				var digits = display.Split(" ");
				var targets = digits.Count(x => x.Length == 2 || x.Length == 4 || x.Length == 3 || x.Length == 7);
				sum += targets;
			}

			return sum.ToString();
		}

		public override string Run2()
		{
			var inputArray = GetInputArray();
			var sum = 0;

			foreach (var input in inputArray)
			{
				Dictionary<string, string> digit = new Dictionary<string, string>();

				var signals = input.Split(" | ")[0].Split(" ");
				var digit4 = input.Split(" | ")[1].Split(" ");

				// unique digits
				var one = signals.First(x => x.Length == 2);
				var four = signals.First(x => x.Length == 4);
				var seven = signals.First(x => x.Length == 3);
				var eight = signals.First(x => x.Length == 7);

				// found digits
				var three = signals.Where(x => x.Length == 5).First(x => x.Contains(one[0]) && x.Contains(one[1]));
				var nine = signals.Where(x => x.Length == 6).First(x => x.Contains(three[0]) && x.Contains(three[1]) && x.Contains(three[2]) &&
				                                                        x.Contains(three[3]) && x.Contains(three[4]));

				var five = signals.Where(x => x.Length == 5)
				                  .First(x => nine.Contains(x[0]) && nine.Contains(x[1]) && nine.Contains(x[2]) && nine.Contains(x[3]) &&
				                              nine.Contains(x[4]) && x != three);

				var two = signals.Where(x => x.Length == 5).First(x => x != three && x != five);

				var six = signals.Where(x => x.Length == 6)
				                 .First(x => x.Contains(five[0]) && x.Contains(five[1]) && x.Contains(five[2]) && x.Contains(five[3]) &&
				                             x.Contains(five[4]) && x != nine);

				var zero = signals.First(x => x != one && x != two && x != three && x != four && x != five && x != six && x != seven && x != eight &&
				                              x != nine);

				digit[string.Join("", one.OrderBy(x => x).ToList())] = "1";
				digit[string.Join("", two.OrderBy(x => x).ToList())] = "2";
				digit[string.Join("", three.OrderBy(x => x).ToList())] = "3";
				digit[string.Join("", four.OrderBy(x => x).ToList())] = "4";
				digit[string.Join("", five.OrderBy(x => x).ToList())] = "5";
				digit[string.Join("", six.OrderBy(x => x).ToList())] = "6";
				digit[string.Join("", seven.OrderBy(x => x).ToList())] = "7";
				digit[string.Join("", eight.OrderBy(x => x).ToList())] = "8";
				digit[string.Join("", nine.OrderBy(x => x).ToList())] = "9";
				digit[string.Join("", zero.OrderBy(x => x).ToList())] = "0";

				var currentSum = Convert.ToInt32(string.Join("", digit4.Select(x => digit[string.Join("", x.OrderBy(x => x).ToList())])));
				sum += currentSum;
			}

			return sum.ToString();
		}
	}
}