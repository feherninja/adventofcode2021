﻿using System;
using AdventOfCode2021.Codebase;

namespace AdventOfCode2021.Missions.Day02
{
	public class Day02 : MissionBase
	{
		public override string Run()
		{
			int horizontal = 0;
			int depth = 0;

			foreach (var order in GetInputArray())
			{
				string direction = order.Split(" ")[0];
				int magnitude = Convert.ToInt32(order.Split(" ")[1]);
				{
					if (direction == "forward")
					{
						horizontal += magnitude;
					}

					if (direction == "up")
					{
						depth -= magnitude;
					}

					if (direction == "down")
					{
						depth += magnitude;
					}
				}
			}

			return $"horizontal = {horizontal}, depth = {depth}, X = {horizontal * depth}";
		}

		public override string Run2()
		{
			int horizontal = 0;
			int depth = 0;
			int aim = 0;

			foreach (var order in GetInputArray())
			{
				string direction = order.Split(" ")[0];
				int magnitude = Convert.ToInt32(order.Split(" ")[1]);
				{
					if (direction == "forward")
					{
						horizontal += magnitude;

						if (aim !=0)
						{
							depth += aim * magnitude;
						}
					}

					if (direction == "up")
					{
						aim -= magnitude;
					}

					if (direction == "down")
					{
						aim += magnitude;
					}
				}
			}

			return $"horizontal = {horizontal}, depth = {depth}, X = {horizontal * depth}";
		}
	}
}