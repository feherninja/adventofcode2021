﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdventOfCode2021.Codebase;

namespace AdventOfCode2021.Missions.Day07
{
	public class Day07 : MissionBase
	{
		public override string Run()
		{
			List<int> crabmarines = GetInput().Split(",").Select(x => Convert.ToInt32(x)).ToList();
			Dictionary<int, int> fuelCosts = new Dictionary<int, int>();

			foreach (var coord in Enumerable.Range(0, crabmarines.Max()))
			{
				Console.Write($"Coord : {coord}");
				int fuel = 0;

				foreach (var crabmarine in crabmarines)
				{
					fuel += Math.Abs(crabmarine - coord);
				}

				fuelCosts[coord] = fuel;
				Console.WriteLine($" -- fuel : {fuel}");
			}

			var leastFuel = fuelCosts.Values.Min();
			var leastCoord = fuelCosts.Keys.First(x => fuelCosts[x] == leastFuel);

			return $"WinCoord : {leastCoord} - fuel: {leastFuel}";
		}

		public override string Run2()
		{
			List<int> crabmarines = GetInput().Split(",").Select(x => Convert.ToInt32(x)).ToList();
			Dictionary<int, int> fuelCosts = new Dictionary<int, int>();

			foreach (var coord in Enumerable.Range(0, crabmarines.Max()))
			{
				Console.Write($"Coord : {coord}");
				int fuel = 0;

				foreach (var crabmarine in crabmarines)
				{
					int coordDelta = Math.Abs(crabmarine - coord);
					int moveCost = 0;

					foreach (var cost in Enumerable.Range(1, coordDelta))
					{
						moveCost += cost;
					}

					fuel += moveCost;
				}

				fuelCosts[coord] = fuel;
				Console.WriteLine($" -- fuel : {fuel}");
			}

			var leastFuel = fuelCosts.Values.Min();
			var leastCoord = fuelCosts.Keys.First(x => fuelCosts[x] == leastFuel);

			return $"WinCoord : {leastCoord} - fuel: {leastFuel}";
		}
	}
}