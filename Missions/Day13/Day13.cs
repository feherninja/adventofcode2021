﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdventOfCode2021.Codebase;
using AdventOfCode2021.Missions.Day13.Models;

namespace AdventOfCode2021.Missions.Day13
{
	public class Day13 : MissionBase
	{
		public override string Run()
		{
			var input = GetInput();
			var inputCoods = input.Split("\r\n\r\n")[0].Split("\r\n");
			var inputFolds = input.Split("\r\n\r\n")[1].Split("\r\n")
			                      .Select(x => new Tuple<string, int>(x.Split(" ")[2].Split("=")[0], Convert.ToInt32(x.Split(" ")[2].Split("=")[1])));

			List<Point> coords = new List<Point>();

			foreach (var line in inputCoods)
			{
				coords.Add(new Point(Convert.ToInt32(line.Split(",")[0]), Convert.ToInt32(line.Split(",")[1])));
			}

			foreach (var fold in inputFolds)
			{
				if (fold.Item1 == "x")
				{
					var exposedPoints = coords.Where(x => x.X > fold.Item2).ToList();

					foreach (var point in exposedPoints)
					{
						int newX = fold.Item2 - Math.Abs(point.X - fold.Item2);

						if (coords.Count(x => x.X == newX && x.Y == point.Y) == 0)
						{
							coords.Add(new Point(newX, point.Y));
						}

						coords.Remove(point);
					}
				}

				if (fold.Item1 == "y")
				{
					var exposedPoints = coords.Where(x => x.Y > fold.Item2).ToList();

					foreach (var point in exposedPoints)
					{
						int newY = fold.Item2 - Math.Abs(point.Y - fold.Item2);

						if (coords.Count(x => x.X == point.X && x.Y == newY) == 0)
						{
							coords.Add(new Point(point.X, newY));
						}

						coords.Remove(point);
					}
				}

				break;
			}

			return coords.Count().ToString();
		}

		public override string Run2()
		{
			var input = GetInput();
			var inputCoods = input.Split("\r\n\r\n")[0].Split("\r\n");
			var inputFolds = input.Split("\r\n\r\n")[1].Split("\r\n")
			                      .Select(x => new Tuple<string, int>(x.Split(" ")[2].Split("=")[0], Convert.ToInt32(x.Split(" ")[2].Split("=")[1])));

			List<Point> coords = new List<Point>();

			foreach (var line in inputCoods)
			{
				coords.Add(new Point(Convert.ToInt32(line.Split(",")[0]), Convert.ToInt32(line.Split(",")[1])));
			}

			foreach (var fold in inputFolds)
			{
				if (fold.Item1 == "x")
				{
					var exposedPoints = coords.Where(x => x.X > fold.Item2).ToList();

					foreach (var point in exposedPoints)
					{
						int newX = fold.Item2 - Math.Abs(point.X - fold.Item2);

						if (coords.Count(x => x.X == newX && x.Y == point.Y) == 0)
						{
							coords.Add(new Point(newX, point.Y));
						}

						coords.Remove(point);
					}
				}

				if (fold.Item1 == "y")
				{
					var exposedPoints = coords.Where(x => x.Y > fold.Item2).ToList();

					foreach (var point in exposedPoints)
					{
						int newY = fold.Item2 - Math.Abs(point.Y - fold.Item2);

						if (coords.Count(x => x.X == point.X && x.Y == newY) == 0)
						{
							coords.Add(new Point(point.X, newY));
						}

						coords.Remove(point);
					}
				}
			}

			displayGrid(coords);
			return coords.Count().ToString();
		}

		private void displayGrid(List<Point> coords)
		{
			Console.Clear();

			for (int i = 0; i <= coords.Max(x => x.Y); i++)
			{
				for (int j = 0; j <= coords.Max(x => x.X); j++)
				{
					if (coords.Count(x => x.X == j && x.Y == i) > 0)
					{
						Console.ForegroundColor = ConsoleColor.DarkGreen;
						Console.Write("#");
					}
					else
					{
						Console.ForegroundColor = ConsoleColor.Black;
						Console.Write(" ");
					}
				}

				Console.WriteLine();
			}
		}
	}
}