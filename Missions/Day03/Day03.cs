﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdventOfCode2021.Codebase;

namespace AdventOfCode2021.Missions.Day03
{
	public class Day03 : MissionBase
	{
		public override string Run()
		{
			var input = GetInputArray();
			string gamma = String.Empty;
			string epsilon = String.Empty;

			for (int i = 0; i < input[0].Length; i++)
			{
				var currentDigitList = input.Select(x => x[i]);

				if (currentDigitList.Count(x => x == '0') > currentDigitList.Count(x => x == '1'))
				{
					gamma += '0';
					epsilon += '1';
				}
				else
				{
					gamma += '1';
					epsilon += '0';
				}
			}

			return
				$"gamma : {Convert.ToInt32(gamma, 2)} ({gamma}) , epsilon : {Convert.ToInt32(epsilon, 2)} ({epsilon}) , consumption: {Convert.ToInt32(gamma, 2) * Convert.ToInt32(epsilon, 2)}";
		}

		public override string Run2()
		{
			var inputOxygen = GetInputArray();
			var inputCo2 = GetInputArray();
			string oxygen = String.Empty;
			string co2 = String.Empty;
			int currentElement = 0;

			while (inputOxygen.Count() > 1)
			{
				filterList(inputOxygen, currentElement, true);
				currentElement++;
			}

			currentElement = 0;

			while (inputCo2.Count() > 1)
			{
				filterList(inputCo2, currentElement, false);
				currentElement++;
			}

			return
				$"O2 : {Convert.ToInt32(inputOxygen[0], 2)} ({inputOxygen[0]}) , Co2 : {Convert.ToInt32(inputCo2[0], 2)} ({inputCo2[0]}) , lifeSupport: {Convert.ToInt32(inputOxygen[0], 2) * Convert.ToInt32(inputCo2[0], 2)}";
		}

		private void filterList(List<string> list, int currentElement, bool mostLeast)
		{
			int count1 = list.Select(x => x[currentElement]).Count(x => x == '1');
			int count0 = list.Select(x => x[currentElement]).Count(x => x == '0');

			char most = count1 >= count0 ? '1' : '0';
			char least = count1 >= count0 ? '0' : '1';

			if (mostLeast)
			{
				list.RemoveAll(x => x[currentElement] == least);
			}
			else
			{
				list.RemoveAll(x => x[currentElement] == most);
			}
		}
	}
}