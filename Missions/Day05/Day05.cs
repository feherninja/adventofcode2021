﻿using System.Collections.Generic;
using System.Linq;
using AdventOfCode2021.Codebase;
using AdventOfCode2021.Missions.Day05.Models;

namespace AdventOfCode2021.Missions.Day05
{
	public class Day05 : MissionBase
	{
		public override string Run()
		{
			List<Line> lines = GetInputArray().Select(x => new Line(x)).ToList();

			List<Line> straightLines = lines.Where(x => x.straight).ToList();
			Dictionary<string, int> pressure = new Dictionary<string, int>();

			foreach (var line in straightLines)
			{
				foreach (var coord in line.GetCoords())
				{
					if (!pressure.ContainsKey(coord))
					{
						pressure.Add(coord, 1);
					}
					else
					{
						pressure[coord]++;
					}
				}
			}

			return $"{pressure.Values.Count(x => x > 1)}";
		}

		public override string Run2()
		{
			List<Line> lines = GetInputArray().Select(x => new Line(x)).ToList();

			Dictionary<string, int> pressure = new Dictionary<string, int>();

			foreach (var line in lines)
			{
				foreach (var coord in line.GetCoords())
				{
					if (!pressure.ContainsKey(coord))
					{
						pressure.Add(coord, 1);
					}
					else
					{
						pressure[coord]++;
					}
				}
			}

			return $"{pressure.Values.Count(x => x > 1)}";
		}
	}
}