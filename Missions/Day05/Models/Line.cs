﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2021.Missions.Day05.Models
{
	public class Line
	{
		public int x1 { get; set; }
		public int y1 { get; set; }
		public int x2 { get; set; }
		public int y2 { get; set; }

		public bool straight => x1 == x2 || y1 == y2;

		public Line(string template)
		{
			var parts = template.Split(" -> ");
			x1 = Convert.ToInt32(parts[0].Split(",")[0]);
			y1 = Convert.ToInt32(parts[0].Split(",")[1]);
			x2 = Convert.ToInt32(parts[1].Split(",")[0]);
			y2 = Convert.ToInt32(parts[1].Split(",")[1]);
		}

		public List<string> GetCoords()
		{
			int delta = 0;
			List<string> coords = new List<string>();

			if (straight)
			{
				if (y1 == y2)
				{
					for (int i =  Math.Min(x1, x2); i <= Math.Max(x1, x2); i++)
					{
						coords.Add($"{i},{y1}");
					}
				}

				if (x1 == x2)
				{
					for (int i = Math.Min(y1, y2); i <= Math.Max(y1, y2); i++)
					{
						coords.Add($"{x1},{i}");
					}
				}
			}
			else
			{
				if (x1<x2 && y1>y2 ||
				    x1>x2 && y1<y2)
				{
					delta = -1;
				}
				else
				{
					delta = 1;
				}
				
				
				for (int i =  Math.Min(x1, x2), j=Math.Min(x1,x2)==x1?y1:y2; i <= Math.Max(x1, x2); i++,j+=delta)
				{
					coords.Add($"{i},{j}");
				}
			}
			
			
			
			
			return coords;
		}
	}
}