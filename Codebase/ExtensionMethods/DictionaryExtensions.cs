﻿using System.Collections.Generic;

namespace AdventOfCode2021.Codebase.ExtensionMethods
{
	public static class DictionaryExtensions
	{
		public static void AddOrUpdate<Tone, Ttwo>(this Dictionary<Tone, Ttwo> dictionary, Tone key, Ttwo value)
		{
			if (dictionary.ContainsKey(key))
			{
				dictionary[key] = value;
			}
			else
			{
				dictionary.Add(key, value);
			}
		}

		public static void AddOrIncrement<T>(this Dictionary<T, double> dictionary, T key)
		{
			if (dictionary.ContainsKey(key))
			{
				dictionary[key]++;
			}
			else
			{
				dictionary.Add(key, 1);
			}
		}

		public static void AddOrIncrementBy<T>(this Dictionary<T, double> dictionary, T key, double value)
		{
			if (dictionary.ContainsKey(key))
			{
				dictionary[key] += value;
			}
			else
			{
				dictionary.Add(key, value);
			}
		}
	}
}