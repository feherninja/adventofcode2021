﻿using System;
using System.IO;
using System.Text;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Modes;
using Org.BouncyCastle.Crypto.Parameters;

#nullable disable

namespace AdventOfCode2021.Codebase.Cookies
{
	public partial class Cookie
	{
		public long CreationUtc { get; set; }
		public string TopFrameSiteKey { get; set; }
		public string HostKey { get; set; }
		public string Name { get; set; }
		public string Value { get; set; }
		public byte[] EncryptedValue { get; set; }
		public string Path { get; set; }
		public long ExpiresUtc { get; set; }
		public long IsSecure { get; set; }
		public long IsHttponly { get; set; }
		public long LastAccessUtc { get; set; }
		public long HasExpires { get; set; }
		public long IsPersistent { get; set; }
		public long Priority { get; set; }
		public long Samesite { get; set; }
		public long SourceScheme { get; set; }
		public long SourcePort { get; set; }
		public long IsSameParty { get; set; }

		public static string DecryptWithKey(byte[] message, byte[] key, int nonSecretPayloadLength)
		{
			const int KEY_BIT_SIZE = 256;
			const int MAC_BIT_SIZE = 128;
			const int NONCE_BIT_SIZE = 96;

			if (key == null || key.Length != KEY_BIT_SIZE / 8)
				throw new ArgumentException(String.Format("Key needs to be {0} bit!", KEY_BIT_SIZE), "key");

			if (message == null || message.Length == 0)
				throw new ArgumentException("Message required!", "message");

			using (var cipherStream = new MemoryStream(message))
			using (var cipherReader = new BinaryReader(cipherStream))
			{
				var nonSecretPayload = cipherReader.ReadBytes(nonSecretPayloadLength);
				var nonce = cipherReader.ReadBytes(NONCE_BIT_SIZE / 8);
				var cipher = new GcmBlockCipher(new AesEngine());
				var parameters = new AeadParameters(new KeyParameter(key), MAC_BIT_SIZE, nonce);
				cipher.Init(false, parameters);
				var cipherText = cipherReader.ReadBytes(message.Length);
				var plainText = new byte[cipher.GetOutputSize(cipherText.Length)];

				try
				{
					var len = cipher.ProcessBytes(cipherText, 0, cipherText.Length, plainText, 0);
					cipher.DoFinal(plainText, len);
				}
				catch (InvalidCipherTextException)
				{
					return null;
				}

				return Encoding.Default.GetString(plainText);
			}
		}
	}
}