﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AdventOfCode2021.Codebase.Cookies
{
    public partial class Metum
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
