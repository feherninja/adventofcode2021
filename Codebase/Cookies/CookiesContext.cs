﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace AdventOfCode2021.Codebase.Cookies
{
    public partial class CookiesContext : DbContext
    {
        private static string GetChromeCookiePath()
        {
            string s = Environment.GetFolderPath(
                Environment.SpecialFolder.LocalApplicationData);
            s += @"\Google\Chrome\User Data\Default\cookies";

            if (!File.Exists(s))
                return string.Empty;

            return s;
        }

        public CookiesContext()
        {
        }

        public CookiesContext(DbContextOptions<CookiesContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cookie> Cookies { get; set; }
        public virtual DbSet<Metum> Meta { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlite($"Data Source={GetChromeCookiePath()}");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cookie>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("cookies");

                entity.HasIndex(e => new { e.TopFrameSiteKey, e.HostKey, e.Name, e.Path }, "IX_cookies_top_frame_site_key_host_key_name_path")
                    .IsUnique();

                entity.Property(e => e.CreationUtc).HasColumnName("creation_utc");

                entity.Property(e => e.EncryptedValue)
                    .HasColumnName("encrypted_value")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.ExpiresUtc).HasColumnName("expires_utc");

                entity.Property(e => e.HasExpires)
                    .HasColumnName("has_expires")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.HostKey)
                    .IsRequired()
                    .HasColumnName("host_key");

                entity.Property(e => e.IsHttponly).HasColumnName("is_httponly");

                entity.Property(e => e.IsPersistent)
                    .HasColumnName("is_persistent")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IsSameParty).HasColumnName("is_same_party");

                entity.Property(e => e.IsSecure).HasColumnName("is_secure");

                entity.Property(e => e.LastAccessUtc).HasColumnName("last_access_utc");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasColumnName("path");

                entity.Property(e => e.Priority)
                    .HasColumnName("priority")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Samesite)
                    .HasColumnName("samesite")
                    .HasDefaultValueSql("-1");

                entity.Property(e => e.SourcePort)
                    .HasColumnName("source_port")
                    .HasDefaultValueSql("-1");

                entity.Property(e => e.SourceScheme).HasColumnName("source_scheme");

                entity.Property(e => e.TopFrameSiteKey)
                    .IsRequired()
                    .HasColumnName("top_frame_site_key");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value");
            });

            modelBuilder.Entity<Metum>(entity =>
            {
                entity.HasKey(e => e.Key);

                entity.ToTable("meta");

                entity.Property(e => e.Key)
                    .HasColumnType("LONGVARCHAR")
                    .HasColumnName("key");

                entity.Property(e => e.Value)
                    .HasColumnType("LONGVARCHAR")
                    .HasColumnName("value");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
