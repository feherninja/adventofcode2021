﻿using AdventOfCode2021.Codebase.Cookies;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Cookie = System.Net.Cookie;

namespace AdventOfCode2021.Codebase
{
	public class MissionBase
	{
		public virtual string GetClassName()
		{
			return this.GetType().Name;
		}

		public async Task<string> GetInputOnline()
		{
			int dayMarker = Convert.ToInt32(GetClassName()[^2..]);

			Dictionary<string, string> AocCookies = new Dictionary<string, string>();

			using (var context = new CookiesContext())
			{
				var cookies = context.Cookies.Where(c => c.HostKey == ".adventofcode.com").ToList();
				foreach (var cookie in cookies)
				{
					var encryptedValue = cookie.EncryptedValue;
					string localAppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
					string encKey = File.ReadAllText(localAppDataPath + @"\Google\Chrome\User Data\Local State");
					encKey = JObject.Parse(encKey)["os_crypt"]["encrypted_key"].ToString();


					var decodedKey = System.Security.Cryptography.ProtectedData.Unprotect(Convert.FromBase64String(encKey).Skip(5).ToArray(), null, System.Security.Cryptography.DataProtectionScope.LocalMachine);
					var clearTextValue = AdventOfCode2021.Codebase.Cookies.Cookie.DecryptWithKey(encryptedValue, decodedKey, 3);

					AocCookies.Add(cookie.Name, clearTextValue);
				}
			}


			var baseAddress = new Uri("https://adventofcode.com");
			var cookieContainer = new CookieContainer();

			foreach (var aocCookie in AocCookies)
			{
				cookieContainer.Add(baseAddress, new Cookie(aocCookie.Key, aocCookie.Value));
			}

			using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
			using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
			{
                
				var result = await client.GetAsync($"/2021/day/{dayMarker}/input");
				return result.Content.ReadAsStringAsync().Result;
			}
		}

		public string GetInput()
		{
			string dayMarker = GetClassName();
			string currentDirectory = Directory.GetCurrentDirectory();
			string filePath = System.IO.Path.Combine(currentDirectory, $@"Inputs\{dayMarker}", $"{dayMarker}Input.txt");
			return File.ReadAllText(filePath);
		}

		public string GetInputSample()
		{
			string dayMarker = GetClassName();
			string currentDirectory = Directory.GetCurrentDirectory();
			string filePath = System.IO.Path.Combine(currentDirectory, $@"Inputs\{dayMarker}", $"{dayMarker}Sample.txt");

			return File.ReadAllText(filePath);
		}

		public List<string> GetInputArray()
		{
			string dayMarker = GetClassName();
			string currentDirectory = Directory.GetCurrentDirectory();
			string filePath = System.IO.Path.Combine(currentDirectory, $@"Inputs\{dayMarker}", $"{dayMarker}Input.txt");

			return File.ReadLines(filePath).ToList();
		}
		
		public List<string> GetInputArraySample()
		{
			string dayMarker = GetClassName();
			string currentDirectory = Directory.GetCurrentDirectory();
			string filePath = System.IO.Path.Combine(currentDirectory, $@"Inputs\{dayMarker}", $"{dayMarker}Sample.txt");

			return File.ReadLines(filePath).ToList();
		}

		public virtual string Run()
		{
			return GetInput();
		}

		public virtual string RunSample()
		{
			return GetInputSample();
		}
		
		public virtual string Run2()
		{
			return GetInput();
		}

		public virtual string Run2Sample()
		{
			return GetInputSample();
		}
	}
}