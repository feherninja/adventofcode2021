﻿using System;
using AdventOfCode2021.Missions;
using AdventOfCode2021.Missions.Day00;

namespace AdventOfCode2021.Codebase
{
	public static class Engine
	{
		public static MissionBase GetTodaysMission(string dayMarker)
		{
			string fqName = $"{typeof(MissionMarker).Namespace}.{dayMarker}.{dayMarker}";
			string reverse = typeof(Day00).AssemblyQualifiedName;
			Type t = Type.GetType(fqName);
			MissionBase Today = (MissionBase) Activator.CreateInstance(t);
			return Today;
		}
	}
}